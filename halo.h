#ifndef HALO_H
#define HALO_H

#define HALO_FORMAT_REVISION 2

#include <stdint.h>

struct halo {
  int64_t id;
  float pos[6], corevel[3], bulkvel[3];
  float m, r, child_r, vmax_r, mgrav, vmax, rvmax, rs, klypin_rs, vrms,
    J[3], energy, spin, alt_m[4], Xoff, Voff, b_to_a, c_to_a, A[3],
    b_to_a2, c_to_a2, A2[3],
    bullock_spin, kin_to_pot, m_pe_b, m_pe_d, halfmass_radius;
  int64_t num_p, num_child_particles, p_start, desc, flags, n_core;
  float min_pos_err, min_vel_err, min_bulkvel_err;

  /* galaxy properties */
  float star_mb[6], m_stars, m_gas, m_bh, m_dm, m_galstars, r_galstars;
  int64_t num_stars, num_gas, num_bh, num_dm;
  float xcm_star[3], xc_pot[3], xc_dm[3], xc_stars[3];
  float f_coarse;
  int64_t num_marked;
};

struct extra_halo_info {
  int64_t child, next_cochild, prev_cochild;
  int64_t sub_of, ph;
  float max_metric;
};

#endif /* HALO_H */
