#ifndef __BUNDLED_FORTRAN_UNFORMATTED_HH
#define __BUNDLED_FORTRAN_UNFORMATTED_HH

#include <stdio.h> 
#include <fcntl.h> 
#include <string.h> 
#include <sys/mman.h> 

#include <string>
#include <fstream>
#include <map>
#include <iostream>

#ifndef DEFAULT_ADDLEN
#define DEFAULT_ADDLEN 4
#endif

/*
// the full tar file header:
struct tar_header
{
	char name[100];
	char mode[8];
	char uid[8];
	char gid[8];
	char size[12];
	char mtime[12];
	char chksum[8];
	char typeflag;
	char linkname[100];
	char magic[6];
	char version[2];
	char uname[32];
	char gname[32];
	char devmajor[8];
	char devminor[8];
	char prefix[155];
	char padding[12];
	char *gnu_longname;
	char *gnu_longlink;
};

 */

class BundledSnapshot{
protected:

  struct tar_header {
    char name[100];
    char _unused[24];
    char size[12];
    char _padding[376];
  };

  struct tar_record{
    std::streampos start;
    unsigned length;

    tar_record( std::streampos start_, unsigned length_ )
      : start( start_ ), length( length_ )
    {}
    tar_record( const tar_record& r )
      : start( r.start ), length( r.length )
    {}
  };


  std::ifstream *pifs_;

  std::string bundle_name_;

  bool constr_from_stream_; 
  
  std::map< std::string, tar_record > record_map_;

  /***/

  void parse_records( void )
  {
    tar_header record;
    unsigned size;
   
    pifs_->seekg(0,std::ios::beg);
    while(1){

      try{
	pifs_->read( reinterpret_cast<char*>( &record ), 512 );
      }catch(...){
	//... check if error or just EOF
	if( pifs_->eof() ){
	  pifs_->clear();
	  pifs_->seekg(0,std::ios::beg);
	  return;
	}
	throw;
      }

      if( pifs_->good() ){
	sscanf( record.size, "%o", &size );
	record_map_.insert( std::pair<std::string,tar_record>( record.name, tar_record( pifs_->tellg(), size ) ) );
	size = ((size+511)/512) * 512;
	pifs_->seekg(size,std::ios::cur);
      }else{
	pifs_->clear();
	pifs_->seekg(0,std::ios::beg);
	break;
      }
    }
  }

public:
  explicit BundledSnapshot( std::ifstream &ifs )
    : pifs_( &ifs ), bundle_name_( "" ), constr_from_stream_(true)
  {
    parse_records();
  }

  explicit BundledSnapshot( std::string bundle_name )
    : bundle_name_( bundle_name ), constr_from_stream_(false)
  {
    pifs_ = new std::ifstream( bundle_name.c_str() );
    parse_records();
  }

  ~BundledSnapshot()
  {
    if( !constr_from_stream_ )
      delete pifs_;
  }

  bool contains_file( std::string fname )
  {
    std::map< std::string, tar_record >::iterator it;
    if( (it=record_map_.find( fname )) == record_map_.end() )
      return false;
    return true;
  }

  void seek_to_file( std::string fname )
  {
    std::map< std::string, tar_record >::iterator it;
    if( (it=record_map_.find( fname )) == record_map_.end() )
      throw std::runtime_error(std::string("Could not find ") + fname + std::string(" in bundle."));
    (*pifs_).seekg( (*it).second.start, std::ios::beg );
  }

  std::streampos end_of_file( std::string fname )
  {
    std::map< std::string, tar_record >::iterator it;
    if( (it=record_map_.find( fname )) == record_map_.end() )
      throw std::runtime_error(std::string("Could not find ") + fname + std::string(" in bundle."));
    return (*it).second.start + (std::streampos)(*it).second.length;
  }

  std::ifstream* get_stream_ptr()
  {
    return pifs_;
  }
};

#endif //  __BUNDLED_FORTRAN_UNFORMATTED_HH
