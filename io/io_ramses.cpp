#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <set>

extern "C"{
#include "io_util.h"
#include "../universal_constants.h"
#include "../check_syscalls.h"
#include "../config_vars.h"
#include "../config.h"
}

#include "io_ramses.h"
#include "ramses/RAMSES_particle_data.hh"

#ifndef RAMSES_DMO
#include "ramses/RAMSES_amr_data.hh"
#include "ramses/RAMSES_hydro_data.hh"

typedef RAMSES::AMR::cell_locally_essential<> RAMSES_cell;
typedef RAMSES::AMR::tree< RAMSES_cell, RAMSES::AMR::level< RAMSES_cell > > RAMSES_tree;
typedef RAMSES::HYDRO::data< RAMSES_tree > RAMSES_hydro_data;
#endif

void load_particles_ramses( char *filename, struct particle **p, int64_t *num_p )
{
  //... open the snapshot info file
  RAMSES::snapshot rsnap( filename, RAMSES::version3 );

  // compute some units
  const double kB      = 1.3806200e-16;
  const double mH      = 1.6600000e-24;

  const double scale_v  = rsnap.m_header.unit_l / rsnap.m_header.unit_t;
  const double scale_T2 = mH / kB * scale_v * scale_v;
  const double scale_m  = rsnap.m_header.unit_d * pow(rsnap.m_header.unit_l,3) 
    * 5.02739933e-34 * rsnap.m_header.H0 * 0.01;
  const double lfac = rsnap.m_header.unit_l / 3.08e24 * rsnap.m_header.H0 * 0.01 / rsnap.m_header.aexp;
  double mfac = scale_m;
  double vfac = rsnap.m_header.unit_l/rsnap.m_header.unit_t * 1e-5;

  // check if run is a hydro or a dmo run
  bool is_hydro = rsnap.have_hydro();
  bool is_hydro_but_do_dmo = is_hydro & (RAMSES_DMONLY>0);

  if( is_hydro )
    fprintf(stdout,"RAMSES run contains hydro data.\n");
  else
    fprintf(stdout,"RAMSES run is N-body only.\n");

  // if we have hydro but want to use N-body only, we need to rescale masses
  const double baryonfac = rsnap.m_header.omega_m/(rsnap.m_header.omega_m-rsnap.m_header.omega_b);
  mfac = is_hydro_but_do_dmo? mfac*baryonfac : mfac;
      
  long long maxid_from_particles = 0;
  size_t global_count = 0; 

  /* read data */
  double min_dm_mass = 1e30;
  size_t nump0 = *num_p;

  std::set<int> idset;
  int sink_dup_counter = (1<<31) + (1<<30);
  int debris_counter   = 1<<29;
 
  for( unsigned icpu=1; icpu<=rsnap.m_header.ncpu; ++icpu )
    {
      RAMSES::PART::data local_data( rsnap, icpu );

      printf("\rReading RAMSES particle data for domain %4d / %4d...",icpu,rsnap.m_header.ncpu);
      fflush(stdout);

      std::vector<float> x, y, z, vx, vy, vz, age, mass;
      std::vector<int> ids;
      
      //        size_t local_particles = 0;
      try{
	local_data.get_var<double>("position_x",std::back_inserter(x));
        
	// we can preallocate memory for the rest
	y.reserve( x.size() );
	z.reserve( x.size() );
	vx.reserve( x.size() );
	vy.reserve( x.size() );
	vz.reserve( x.size() );
	ids.reserve( x.size() );
	mass.reserve( x.size() );
        
	local_data.get_var<double>("position_y",std::back_inserter(y));
	local_data.get_var<double>("position_z",std::back_inserter(z));
	local_data.get_var<double>("velocity_x",std::back_inserter(vx));
	local_data.get_var<double>("velocity_y",std::back_inserter(vy));
	local_data.get_var<double>("velocity_z",std::back_inserter(vz));
        
	//... and the particle ages
	
	local_data.get_var<int>("particle_ID",std::back_inserter(ids));
	local_data.get_var<double>("mass",std::back_inserter(mass));

	//.. age field will only be present if we have a hydro run
	//.. if we do only n-body then we also need to rescale the masses
	if( is_hydro ){
	  age.reserve( x.size() );
	  local_data.get_var<double>("age",std::back_inserter(age));

	  //if( is_hydro_but_do_dmo ){
	  //  for( size_t i=0; i<mass.size(); ++i )
	  //    mass[i] /= mrescale;
	  //}
	}

	//.. count all particles
	size_t local_particles = 0;
	
	if( !is_hydro_but_do_dmo ){
	  // then count all particles with nonzero mass
	  for( size_t i=0; i<x.size(); ++i )
	    if( fabs(mass[i])*mfac > 1e-10 )
	      local_particles++;
	}else{
	  // then count only DM particles
	  for( size_t i=0; i<x.size(); ++i )
	    if( fabs(mass[i])*mfac > 1e-10 
		&& RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_dm) )
	      local_particles++;
	}

	// reallocate
	*p = (struct particle *)check_realloc(*p, ((*num_p)+local_particles)*sizeof(struct particle), 
					      "Allocating particles.");


	for( size_t i=0,j=0; i<x.size(); ++i )
	  {
	    if( fabs(mass[i])*mfac > 1e-10 ){ 

	      if( is_hydro_but_do_dmo 
		  && !RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_dm) )
		continue;

	      (*p)[*num_p+j].pos[0] = x[i] * lfac;
	      (*p)[*num_p+j].pos[1] = y[i] * lfac;
	      (*p)[*num_p+j].pos[2] = z[i] * lfac;
	      (*p)[*num_p+j].pos[3] = vx[i] * vfac;
	      (*p)[*num_p+j].pos[4] = vy[i] * vfac;
	      (*p)[*num_p+j].pos[5] = vz[i] * vfac;
	      (*p)[*num_p+j].id     = ids[i];//global_count++;//ids[i];
	      (*p)[*num_p+j].mass   = fabs(mass[i]) * mfac;
	      (*p)[*num_p+j].energy = 0.0;

	      if( !is_hydro ){
		(*p)[*num_p+j].type = RTYPE_DM;
		if( mass[i]*mfac < min_dm_mass )
		  min_dm_mass = mass[i]*mfac;
	      }else{
		if( RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_dm) ){
		  (*p)[*num_p+j].type = RTYPE_DM;
		  if( mass[i]*mfac < min_dm_mass )
		    min_dm_mass = mass[i]*mfac;
		}else if( RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_star) ){
		  (*p)[*num_p+j].type = RTYPE_STAR;
		  (*p)[*num_p+j].id   = ids[i] + (1<<30);
		}else if( RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_debris) ){
		  (*p)[*num_p+j].type = RTYPE_OTHER;
		  (*p)[*num_p+j].id   = debris_counter++;
		}else if( RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_sink) ){
		  (*p)[*num_p+j].type = RTYPE_BH;
		  if( idset.find( ids[i] ) != idset.end() ) {
		    (*p)[*num_p+j].id = sink_dup_counter--;
		    idset.insert( (*p)[*num_p+j].id );
		  }else{
		    idset.insert( ids[i] );
		  }
		}else{
		  (*p)[*num_p+j].type = RTYPE_OTHER;
		}
	      }

	      if( ids[i] > maxid_from_particles )
		maxid_from_particles = ids[i];

	      ++j;
	    }
	    
	  }

	*num_p += local_particles;
        
      }catch(...)
        {
	  //... then some read operation failed
	  std::cerr << "something bad happened while operating on domain #" << icpu << ".\n";
	  throw;
        }
    }

  printf("\n");

  //maxid_from_particles = global_count+1;

  if( is_hydro && !is_hydro_but_do_dmo ){

    /* now also add leaf cells of all hydro data */
    size_t global_cell_count = 0;

    for( unsigned icpu=1; icpu<=rsnap.m_header.ncpu; ++icpu )
      {
	printf("\rReading RAMSES grid data for domain %4d / %4d...",icpu,rsnap.m_header.ncpu);
	fflush(stdout);
	
	unsigned minlvl = 1, maxlvl = rsnap.m_header.levelmax;
	RAMSES_tree local_tree( rsnap, icpu, maxlvl, minlvl );
	local_tree.read();
	
	RAMSES_hydro_data local_data( local_tree );
	local_data.read(); 
	/*  local_density_data( local_tree ),
	    local_pressure_data( local_tree ),
	    local_vx_data( local_tree ),
	    local_vy_data( local_tree ),
	    local_vz_data( local_tree );
	    
	    local_density_data.read( "density" );
	    local_vx_data.read( "velocity_x" );
	    local_vy_data.read( "velocity_y" );
	    local_vz_data.read( "velocity_z" );
	    local_pressure_data.read( "pressure" );*/
	
	size_t leaf_count = 0;

	for( unsigned ilevel = rsnap.m_header.levelmin; ilevel < rsnap.m_header.levelmax; ++ilevel )
	  {
	    RAMSES_tree::iterator grid_it = local_tree.begin( ilevel );
	    
	    // count leaf cells only
	    while( grid_it!=local_tree.end(ilevel) ){
	      if( (unsigned)grid_it.get_domain() == icpu ){						
		for( int i=0; i<8; i++ )
		  if( !grid_it.is_refined(i) || ilevel == rsnap.m_header.levelmax-1 )
		    ++leaf_count;
	      }
	      ++grid_it;
	    }
	  }
	
	*p = (struct particle *)check_realloc(*p, ((*num_p)+leaf_count)*sizeof(struct particle), 
					      "Allocating particles.");
	leaf_count = 0;
	
	for( unsigned ilevel = rsnap.m_header.levelmin; ilevel < rsnap.m_header.levelmax; ++ilevel )
	  {
	    double dx = 1.0/(1ull<<(ilevel+1)), dx3 = dx*dx*dx;
	    
	    RAMSES_tree::iterator grid_it = local_tree.begin( ilevel );
	    
	    // count leaf cells only
	    while( grid_it!=local_tree.end(ilevel) ){
	      
	      //... is current grid a local grid? ...
	      if( (unsigned)grid_it.get_domain() == icpu ){
		
		//... loop over its cells ...
		for( int i=0; i<8; i++ ){
		  
		  //... are they further refined or have we reached the max. refinement level?
		  if( !grid_it.is_refined(i) || ilevel == rsnap.m_header.levelmax-1 ){
		    //... then is a leaf cell or @ max refine, 
		    //... determine its position ...
		    RAMSES::AMR::vec<double> pos;
		    pos = local_tree.cell_pos<double>( grid_it, i );
		    
		    double rho = local_data.cell_value( grid_it, RAMSES::density, i );
		    double vx  = local_data.cell_value( grid_it, RAMSES::velocity_x, i );
		    double vy  = local_data.cell_value( grid_it, RAMSES::velocity_y, i );
		    double vz  = local_data.cell_value( grid_it, RAMSES::velocity_z, i );
		    double P = local_data.cell_value( grid_it, RAMSES::pressure, i );
		    double T = P/rho * scale_T2; // in K/mu
		    double u = 0.0124 * T; // in (km/s)**2
		    
		    double cellmass = rho * dx3 * scale_m;
		    		    
		    double jitamp = 0.05; // add jitter by 5% of the cell size
		    double jitx, jity, jitz;
		    jitx = 0.0;//2.0*((double)random()/RAND_MAX - 1) * dx * jitamp;
		    jity = 0.0;//2.0*((double)random()/RAND_MAX - 1) * dx * jitamp;
		    jitz = 0.0;//2.0*((double)random()/RAND_MAX - 1) * dx * jitamp;
		    
		    (*p)[*num_p+leaf_count].pos[0] = (pos.x + jitx) * lfac;
		    (*p)[*num_p+leaf_count].pos[1] = (pos.y + jity) * lfac;
		    (*p)[*num_p+leaf_count].pos[2] = (pos.z + jitz) * lfac;
		    (*p)[*num_p+leaf_count].pos[3] = vx * vfac;
		    (*p)[*num_p+leaf_count].pos[4] = vy * vfac;
		    (*p)[*num_p+leaf_count].pos[5] = vz * vfac;
		    (*p)[*num_p+leaf_count].id     = maxid_from_particles + (++global_cell_count);
		    (*p)[*num_p+leaf_count].mass   = cellmass;
		    (*p)[*num_p+leaf_count].energy = u;
		    (*p)[*num_p+leaf_count].type = RTYPE_GAS;
		    
		    //if( 0 && ilevel == 18 )
		    //  printf("level = %d, pos = %f,%f,%f  vel = %f,%f,%f   id = %d,  mass = %g, rho = %g, u = %g\n",
		    //  ilevel, pos.x * lfac, pos.y * lfac, pos.z * lfac, 
		    //  vx * vfac, vy * vfac, vz * vfac,
		    //	     maxid_from_particles + global_cell_count, cellmass, rho, u );
		    
		    ++leaf_count;
		  }
		}
	      }
	      ++grid_it;
	    }
	  }
	*num_p += leaf_count;
      }
  }
  
  /* set the code parameters */
  Ol = rsnap.m_header.omega_l;
  Om = rsnap.m_header.omega_m;
  h0 = rsnap.m_header.H0 * 0.01;
  BOX_SIZE = 1.0 * lfac;
  SCALE_NOW = rsnap.m_header.aexp;
 
  if( !PARTICLE_MASS )
    PARTICLE_MASS = min_dm_mass;
  if( !FORCE_RES )
    FORCE_RES = BOX_SIZE / pow(2.0, rsnap.m_header.levelmax) / SCALE_NOW;
  if( !FORCE_RES_PHYS_MAX )
    FORCE_RES_PHYS_MAX = FORCE_RES * SCALE_NOW;

  if( !is_hydro || is_hydro_but_do_dmo )
    AVG_PARTICLE_SPACING = cbrt(min_dm_mass / (Om*CRITICAL_DENSITY));
  else
    AVG_PARTICLE_SPACING = cbrt(min_dm_mass*baryonfac / (Om*CRITICAL_DENSITY));

  TOTAL_PARTICLES = *num_p - nump0;
  
  printf("\n");
  printf("Read %ld particles from RAMSES snapshot.\n\nSimulation stats are:\n",*num_p - nump0);
  printf("  box size = %f,\n  minmass  = %g,\n  <dx>     = %g,\n  force    = %g\n", BOX_SIZE,min_dm_mass, AVG_PARTICLE_SPACING, FORCE_RES );
  
}


void load_particles_ramses_withmark( char *filename, char *markfilename,struct particle **p, int64_t *num_p )
{
  // here's where we want to open the mark file, too
  FILE *markedfile = fopen(markfilename, "rb");
  int marked=0;
 
  // attributeDataI now contains whether the particle is marked or not.



  //... open the snapshot info file
  RAMSES::snapshot rsnap( filename, RAMSES::version3 );

  // compute some units
  const double kB      = 1.3806200e-16;
  const double mH      = 1.6600000e-24;

  const double scale_v  = rsnap.m_header.unit_l / rsnap.m_header.unit_t;
  const double scale_T2 = mH / kB * scale_v * scale_v;
  const double scale_m  = rsnap.m_header.unit_d * pow(rsnap.m_header.unit_l,3) 
    * 5.02739933e-34 * rsnap.m_header.H0 * 0.01;
  const double lfac = rsnap.m_header.unit_l / 3.08e24 * rsnap.m_header.H0 * 0.01 / rsnap.m_header.aexp;
  double mfac = scale_m;
  double vfac = rsnap.m_header.unit_l/rsnap.m_header.unit_t * 1e-5;

  // check if run is a hydro or a dmo run
  bool is_hydro = rsnap.have_hydro();
  bool is_hydro_but_do_dmo = is_hydro & (RAMSES_DMONLY>0);

  if( is_hydro )
    fprintf(stdout,"RAMSES run contains hydro data.\n");
  else
    fprintf(stdout,"RAMSES run is N-body only.\n");

  // if we have hydro but want to use N-body only, we need to rescale masses
  const double baryonfac = rsnap.m_header.omega_m/(rsnap.m_header.omega_m-rsnap.m_header.omega_b);
  mfac = is_hydro_but_do_dmo? mfac*baryonfac : mfac;
      
  long long maxid_from_particles = 0;
  size_t global_count = 0; 

  /* read data */
  double min_dm_mass = 1e30;
  size_t nump0 = *num_p;

  std::set<int> idset;
  int sink_dup_counter = (1<<31) + (1<<30);
  int debris_counter   = 1<<29;
 
  for( unsigned icpu=1; icpu<=rsnap.m_header.ncpu; ++icpu )
    {
      RAMSES::PART::data local_data( rsnap, icpu );

      printf("\rReading RAMSES particle data for domain %4d / %4d...",icpu,rsnap.m_header.ncpu);
      fflush(stdout);

      std::vector<float> x, y, z, vx, vy, vz, age, mass;
      std::vector<int> ids;
      
      //        size_t local_particles = 0;
      try{
	local_data.get_var<double>("position_x",std::back_inserter(x));
        
	// we can preallocate memory for the rest
	y.reserve( x.size() );
	z.reserve( x.size() );
	vx.reserve( x.size() );
	vy.reserve( x.size() );
	vz.reserve( x.size() );
	ids.reserve( x.size() );
	mass.reserve( x.size() );
        
	local_data.get_var<double>("position_y",std::back_inserter(y));
	local_data.get_var<double>("position_z",std::back_inserter(z));
	local_data.get_var<double>("velocity_x",std::back_inserter(vx));
	local_data.get_var<double>("velocity_y",std::back_inserter(vy));
	local_data.get_var<double>("velocity_z",std::back_inserter(vz));
        
	//... and the particle ages
	
	local_data.get_var<int>("particle_ID",std::back_inserter(ids));
	local_data.get_var<double>("mass",std::back_inserter(mass));

	//.. age field will only be present if we have a hydro run
	//.. if we do only n-body then we also need to rescale the masses
	if( is_hydro ){
	  age.reserve( x.size() );
	  local_data.get_var<double>("age",std::back_inserter(age));

	  //if( is_hydro_but_do_dmo ){
	  //  for( size_t i=0; i<mass.size(); ++i )
	  //    mass[i] /= mrescale;
	  //}
	}

	//.. count all particles
	size_t local_particles = 0;
	
	if( !is_hydro_but_do_dmo ){
	  // then count all particles with nonzero mass
	  for( size_t i=0; i<x.size(); ++i )
	    if( fabs(mass[i])*mfac > 1e-10 )
	      local_particles++;
	}else{
	  // then count only DM particles
	  for( size_t i=0; i<x.size(); ++i )
	    if( fabs(mass[i])*mfac > 1e-10 
		&& RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_dm) )
	      local_particles++;
	}

	// reallocate
	*p = (struct particle *)check_realloc(*p, ((*num_p)+local_particles)*sizeof(struct particle), 
					      "Allocating particles.");


	for( size_t i=0,j=0; i<x.size(); ++i )
	  {
	    if( fabs(mass[i])*mfac > 1e-10 ){ 

	      if( is_hydro_but_do_dmo 
		  && !RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_dm) )
		continue;

	      (*p)[*num_p+j].pos[0] = x[i] * lfac;
	      (*p)[*num_p+j].pos[1] = y[i] * lfac;
	      (*p)[*num_p+j].pos[2] = z[i] * lfac;
	      (*p)[*num_p+j].pos[3] = vx[i] * vfac;
	      (*p)[*num_p+j].pos[4] = vy[i] * vfac;
	      (*p)[*num_p+j].pos[5] = vz[i] * vfac;
	      (*p)[*num_p+j].id     = ids[i];//global_count++;//ids[i];
	      (*p)[*num_p+j].mass   = fabs(mass[i]) * mfac;
	      (*p)[*num_p+j].energy = 0.0;


	      // we seek to the global id location
	      fseek(markedfile, sizeof(int)*(ids[i]+1) , SEEK_SET); // plus one as we want to skip the beginning line of the file which is the number of particles!                                                                                                                                                                                                                     error = fread(&marked, sizeof(marked),1, markedfile);
	      (*p)[*num_p+j].marked = marked;

	      if( !is_hydro ){
		(*p)[*num_p+j].type = RTYPE_DM;
		if( mass[i]*mfac < min_dm_mass )
		  min_dm_mass = mass[i]*mfac;
	      }else{
		if( RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_dm) ){
		  (*p)[*num_p+j].type = RTYPE_DM;
		  if( mass[i]*mfac < min_dm_mass )
		    min_dm_mass = mass[i]*mfac;
		}else if( RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_star) ){
		  (*p)[*num_p+j].type = RTYPE_STAR;
		  (*p)[*num_p+j].id   = ids[i] + (1<<30);
		}else if( RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_debris) ){
		  (*p)[*num_p+j].type = RTYPE_OTHER;
		  (*p)[*num_p+j].id   = debris_counter++;
		}else if( RAMSES::PART::is_of_type( age[i], ids[i], RAMSES::PART::ptype_sink) ){
		  (*p)[*num_p+j].type = RTYPE_BH;
		  if( idset.find( ids[i] ) != idset.end() ) {
		    (*p)[*num_p+j].id = sink_dup_counter--;
		    idset.insert( (*p)[*num_p+j].id );
		  }else{
		    idset.insert( ids[i] );
		  }
		}else{
		  (*p)[*num_p+j].type = RTYPE_OTHER;
		}
	      }

	      if( ids[i] > maxid_from_particles )
		maxid_from_particles = ids[i];

	      ++j;
	    }
	    
	  }

	*num_p += local_particles;
        
      }catch(...)
        {
	  //... then some read operation failed
	  std::cerr << "something bad happened while operating on domain #" << icpu << ".\n";
	  throw;
        }
    }

  printf("\n");

  //maxid_from_particles = global_count+1;

  if( is_hydro && !is_hydro_but_do_dmo ){

    /* now also add leaf cells of all hydro data */
    size_t global_cell_count = 0;

    for( unsigned icpu=1; icpu<=rsnap.m_header.ncpu; ++icpu )
      {
	printf("\rReading RAMSES grid data for domain %4d / %4d...",icpu,rsnap.m_header.ncpu);
	fflush(stdout);
	
	unsigned minlvl = 1, maxlvl = rsnap.m_header.levelmax;
	RAMSES_tree local_tree( rsnap, icpu, maxlvl, minlvl );
	local_tree.read();
	
	RAMSES_hydro_data local_data( local_tree );
	local_data.read(); 
	/*  local_density_data( local_tree ),
	    local_pressure_data( local_tree ),
	    local_vx_data( local_tree ),
	    local_vy_data( local_tree ),
	    local_vz_data( local_tree );
	    
	    local_density_data.read( "density" );
	    local_vx_data.read( "velocity_x" );
	    local_vy_data.read( "velocity_y" );
	    local_vz_data.read( "velocity_z" );
	    local_pressure_data.read( "pressure" );*/
	
	size_t leaf_count = 0;

	for( unsigned ilevel = rsnap.m_header.levelmin; ilevel < rsnap.m_header.levelmax; ++ilevel )
	  {
	    RAMSES_tree::iterator grid_it = local_tree.begin( ilevel );
	    
	    // count leaf cells only
	    while( grid_it!=local_tree.end(ilevel) ){
	      if( (unsigned)grid_it.get_domain() == icpu ){						
		for( int i=0; i<8; i++ )
		  if( !grid_it.is_refined(i) || ilevel == rsnap.m_header.levelmax-1 )
		    ++leaf_count;
	      }
	      ++grid_it;
	    }
	  }
	
	*p = (struct particle *)check_realloc(*p, ((*num_p)+leaf_count)*sizeof(struct particle), 
					      "Allocating particles.");
	leaf_count = 0;
	
	for( unsigned ilevel = rsnap.m_header.levelmin; ilevel < rsnap.m_header.levelmax; ++ilevel )
	  {
	    double dx = 1.0/(1ull<<(ilevel+1)), dx3 = dx*dx*dx;
	    
	    RAMSES_tree::iterator grid_it = local_tree.begin( ilevel );
	    
	    // count leaf cells only
	    while( grid_it!=local_tree.end(ilevel) ){
	      
	      //... is current grid a local grid? ...
	      if( (unsigned)grid_it.get_domain() == icpu ){
		
		//... loop over its cells ...
		for( int i=0; i<8; i++ ){
		  
		  //... are they further refined or have we reached the max. refinement level?
		  if( !grid_it.is_refined(i) || ilevel == rsnap.m_header.levelmax-1 ){
		    //... then is a leaf cell or @ max refine, 
		    //... determine its position ...
		    RAMSES::AMR::vec<double> pos;
		    pos = local_tree.cell_pos<double>( grid_it, i );
		    
		    double rho = local_data.cell_value( grid_it, RAMSES::density, i );
		    double vx  = local_data.cell_value( grid_it, RAMSES::velocity_x, i );
		    double vy  = local_data.cell_value( grid_it, RAMSES::velocity_y, i );
		    double vz  = local_data.cell_value( grid_it, RAMSES::velocity_z, i );
		    double P = local_data.cell_value( grid_it, RAMSES::pressure, i );
		    double T = P/rho * scale_T2; // in K/mu
		    double u = 0.0124 * T; // in (km/s)**2
		    
		    double cellmass = rho * dx3 * scale_m;
		    		    
		    double jitamp = 0.05; // add jitter by 5% of the cell size
		    double jitx, jity, jitz;
		    jitx = 0.0;//2.0*((double)random()/RAND_MAX - 1) * dx * jitamp;
		    jity = 0.0;//2.0*((double)random()/RAND_MAX - 1) * dx * jitamp;
		    jitz = 0.0;//2.0*((double)random()/RAND_MAX - 1) * dx * jitamp;
		    
		    (*p)[*num_p+leaf_count].pos[0] = (pos.x + jitx) * lfac;
		    (*p)[*num_p+leaf_count].pos[1] = (pos.y + jity) * lfac;
		    (*p)[*num_p+leaf_count].pos[2] = (pos.z + jitz) * lfac;
		    (*p)[*num_p+leaf_count].pos[3] = vx * vfac;
		    (*p)[*num_p+leaf_count].pos[4] = vy * vfac;
		    (*p)[*num_p+leaf_count].pos[5] = vz * vfac;
		    (*p)[*num_p+leaf_count].id     = maxid_from_particles + (++global_cell_count);
		    (*p)[*num_p+leaf_count].mass   = cellmass;
		    (*p)[*num_p+leaf_count].energy = u;
		    (*p)[*num_p+leaf_count].type = RTYPE_GAS;
		    
		    //if( 0 && ilevel == 18 )
		    //  printf("level = %d, pos = %f,%f,%f  vel = %f,%f,%f   id = %d,  mass = %g, rho = %g, u = %g\n",
		    //  ilevel, pos.x * lfac, pos.y * lfac, pos.z * lfac, 
		    //  vx * vfac, vy * vfac, vz * vfac,
		    //	     maxid_from_particles + global_cell_count, cellmass, rho, u );
		    
		    ++leaf_count;
		  }
		}
	      }
	      ++grid_it;
	    }
	  }
	*num_p += leaf_count;
      }
  }
  
  /* set the code parameters */
  Ol = rsnap.m_header.omega_l;
  Om = rsnap.m_header.omega_m;
  h0 = rsnap.m_header.H0 * 0.01;
  BOX_SIZE = 1.0 * lfac;
  SCALE_NOW = rsnap.m_header.aexp;
 
  if( !PARTICLE_MASS )
    PARTICLE_MASS = min_dm_mass;
  if( !FORCE_RES )
    FORCE_RES = BOX_SIZE / pow(2.0, rsnap.m_header.levelmax) / SCALE_NOW;
  if( !FORCE_RES_PHYS_MAX )
    FORCE_RES_PHYS_MAX = FORCE_RES * SCALE_NOW;

  if( !is_hydro || is_hydro_but_do_dmo )
    AVG_PARTICLE_SPACING = cbrt(min_dm_mass / (Om*CRITICAL_DENSITY));
  else
    AVG_PARTICLE_SPACING = cbrt(min_dm_mass*baryonfac / (Om*CRITICAL_DENSITY));

  TOTAL_PARTICLES = *num_p - nump0;
  
  printf("\n");
  printf("Read %ld particles from RAMSES snapshot.\n\nSimulation stats are:\n",*num_p - nump0);
  printf("  box size = %f,\n  minmass  = %g,\n  <dx>     = %g,\n  force    = %g\n", BOX_SIZE,min_dm_mass, AVG_PARTICLE_SPACING, FORCE_RES );
  
}
