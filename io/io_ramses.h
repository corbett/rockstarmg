#ifndef _IO_RAMSES_H_
#define _IO_RAMSES_H_

#include <stdint.h>
#include "../particle.h"

#if defined(__cplusplus)
extern "C" {
#endif

void load_particles_ramses( char *filename, struct particle **p, int64_t *num_p );
void load_particles_ramses_withmark( char *filename, char *markfilename, struct particle **p, int64_t *num_p );
#if defined(__cplusplus)
}
#endif

#endif /* _IO_GADGET_H_ */
