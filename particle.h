#ifndef PARTICLE_H
#define PARTICLE_H
#include <stdint.h>

#define RTYPE_DM   0
#define RTYPE_GAS  1
#define RTYPE_STAR 2
#define RTYPE_BH   3
#define RTYPE_OTHER 4

struct particle {
  int64_t id;
  float pos[6];
  float mass, energy; /*Energy per unit mass*/
  float softening; /* Per-particle softening, not currently used. */
  int32_t type;
  int32_t marked; /* 1 if marked, 0 otherwise */

};

#endif /*PARTICLE_H*/
